import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Tablero extends Actor
{
    private GreenfootImage imagen;
    private int contador;
    private String mensaje;

    //contructor

    public Tablero(int contador, String mensaje)
    {
        this.contador = contador;
        this.mensaje = mensaje;
        this.imagen = new GreenfootImage(250,150);
        funcionalidad();
    }

    //getter  and setter

    public GreenfootImage getImagen()
    {
        return this.imagen;
    }

    public void setImagen(GreenfootImage imagen)
    {
        this.imagen = imagen;
    }

    public int getContador()
    {
        return this.contador;
    }

    public void setContador(int contador)
    {
        this.contador = contador;
    }

    public String getMensaje()
    {
        return this.mensaje;
    }

    public void setMensaje(String mensaje)
    {
        this.mensaje = mensaje;
    }

    public void act() 
    {

    }    

    //Procediento de funcionalidad general
    private void funcionalidad()
    {
        Color color = getImage().getColor();
        this.imagen.setColor(color.YELLOW);
        dibujar();
    }

    //Procedimeitno para dibujar los mensjaes enviados a la instancia de tipo tablero
    private void dibujar()
    {
        imagen.clear();
        imagen.drawString(getMensaje()+": "+getContador(), 20, 20);
        setImage(imagen);
    }

    //Procedimeinto para sumar 1 a la varible contador
    public void incrementar()
    {
        this.contador++;
        dibujar();
    }

    //procedimiento para restar 1 a ña variable contador
    public void decrementar()
    {
        this.contador--;
        dibujar();
    }
}
