import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class roca here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class roca extends Actor
{
    /**
     * Act - do whatever the roca wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        movimientoRoca();
        choque();
        
    }
    
    //choque de las rocas contra la nave
    
    private void choque()
    {
        Actor nave = getOneObjectAtOffset(0, 0, nave.class);
        if(nave != null){
            Espacio Espacio = (Espacio)getWorld();
            getWorld().removeObject(nave);
            getWorld().addObject(new nave(), 512, 490);
            Espacio.getObjetoLife().Disminuir();
        }
    }
    
    
    //Movimeinto de rocas aleatorios
    public void movimientoRoca()
    {
        move(5);
        World mundo = getWorld();
        if(getX() >= mundo.getWidth()-5 || getX() <=5){
            turn(180);
            if(Greenfoot.getRandomNumber(100)<90){
                turn(Greenfoot.getRandomNumber(90-45));
            }
        }
        if(getY() >= mundo.getWidth()-5 || getY() <=5){
            turn(180);
            if(Greenfoot.getRandomNumber(100)<90){
                turn(Greenfoot.getRandomNumber(90-45));
            }
        }
        
        Actor nave = getOneObjectAtOffset(0, 0, nave.class);
        if(nave != null)
        {
            Espacio espacio = (Espacio)getWorld();
            getWorld().removeObject(nave);
            espacio.getVidas().decrementar();
            getWorld().addObject(nave, 300, 390);
            if(espacio.getVidas().getContador() == 0)
            {
                JuegoTerminado texto = new JuegoTerminado();
                getWorld().addObject(texto, (getWorld().getWidth()/2)+150, (getWorld().getHeight()/2));
                }
        }
    }
}
