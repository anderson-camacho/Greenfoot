import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)


public class JuegoTerminado extends Actor
{
    GreenfootImage imagen;
    String mensaje;
    
    public JuegoTerminado()
    {
        imagen = new GreenfootImage(512,390);
        Color color = getImage().getColor();
        imagen.setColor(color.YELLOW);
        imagen.drawString("Juego Terminado", 0, 100);
        setImage(imagen);
        Greenfoot.stop();
    }
    
    //Getter an d setter
    
    public GreenfootImage getImagen()
    {
        return this.imagen;
    }

    public void setImagen(GreenfootImage imagen)
    {
        this.imagen = imagen;
    }

    public String getMensaje()
    {
        return this.mensaje;
    }

    public void setMensaje(String mensaje)
    {
        this.mensaje = mensaje;
    }
    
    public void act() 
    {
        // Add your action code here.
    }    
}
